package com.pksheldon4.fileuploaddemo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Metadata {

    private String firstName;
    private String lastName;
}

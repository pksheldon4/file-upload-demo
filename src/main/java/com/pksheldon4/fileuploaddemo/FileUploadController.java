package com.pksheldon4.fileuploaddemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Slf4j
public class FileUploadController {

    @PostMapping(value = "/media", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public void uploadFile(@RequestPart("metadata") Metadata metadata,
                           @RequestPart("file") MultipartFile file) {
        log.info("File successfully Uploaded: " + file.getOriginalFilename());
        log.info("Payload: " + metadata);
    }

    @PostMapping(value = "/testjson", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.TEXT_PLAIN_VALUE)
    public String testJson(@RequestBody String testvalue) {
        log.info("####### Hello {}", testvalue);
        return "Yeah!";
    }

    @PostMapping(value = "/testmultipart", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public void testMultipart(@RequestPart("address") String address,
                              @RequestPart("metadata") Metadata metadata) {
        log.info("Address: " + address);
        log.info("Name: " + metadata.toString());
    }


}

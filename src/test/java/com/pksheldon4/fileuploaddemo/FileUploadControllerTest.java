package com.pksheldon4.fileuploaddemo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
class FileUploadControllerTest {

    private MockMvc mvc;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(new FileUploadController()).build();
    }

    @Test
    void uploadFile(@Value("classpath:test-file.txt") Resource testResource,
                    @Value("classpath:metadata.json") Resource jsonResource) throws Exception {

        MockMultipartFile jsonFile = new MockMultipartFile("metadata", "matadata.json", MediaType.APPLICATION_JSON_VALUE, jsonResource.getInputStream().readAllBytes());
        MockMultipartFile testFile = new MockMultipartFile("file", "test-file.txt", MediaType.APPLICATION_OCTET_STREAM_VALUE, testResource.getInputStream());
        mvc.perform(multipart("/media")
            .file(jsonFile)
            .file(testFile)
            .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isOk());
    }

    @Test
    void testJson() throws Exception {
        String testValue = "{\"value\":\"Test\"}";
        mvc.perform(post("/testjson")
            .content(testValue).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    void testMultipart(@Value("classpath:metadata.json") Resource jsonResource) throws Exception {
        String testValue = "{\"value\":\"Test\"}";
        MockMultipartFile jsonFile = new MockMultipartFile("metadata", "matadata.json", MediaType.APPLICATION_JSON_VALUE, jsonResource.getInputStream().readAllBytes());
        MockMultipartFile textData = new MockMultipartFile("address", "address", MediaType.TEXT_PLAIN_VALUE, "Test Address".getBytes());

        mvc.perform(multipart("/testmultipart")
            .file(textData)
            .file(jsonFile)
            .contentType(MediaType.MULTIPART_FORM_DATA))
            .andExpect(status().isOk());
    }
}